<?php

namespace App\Repositories;

use App\Repositories\Repository;
use Twilio\Rest\Client;
use App\Models\VerificationService;

class SMSRepository extends Repository
{
	/**
	* @var SID
	*/
	protected $SID;

	/**
	* @var TOKEN
	*/
	protected $TOKEN;

	/**
	* @var SERVICE_NAME
	*/
	protected $SERVICE_NAME;

	public function __construct($service="NINETY SIX GROUP") {
		$this->SERVICE_NAME = $service;
		$this->SID = env('TWILIO_ACCOUNT_SID', '');
		$this->TOKEN = env('TWILIO_AUTH_TOKEN', '');
	}

	/**
	* @function getService
	* @return string
	*/
    protected function getService() {
    	$twilio = new Client($this->SID, $this->TOKEN);
		$service = $twilio->verify->v2
			->services
			->create($this->SERVICE_NAME);
		
		return $service->sid;
    }

    /**
	* @function sendVerificationToken
	* @return string
	*/
    public function sendVerificationToken($recepient) {
    	$twilio = new Client($this->SID, $this->TOKEN);
    	$serviceId = $this->getService();

    	// Delete existing recepient
    	VerificationService::where('recepient', $recepient)->delete();
		
		// Add new recepient
		$vs = new VerificationService;
		$vs->recepient = $recepient;
		$vs->service = $this->SERVICE_NAME;
		$vs->service_id = $serviceId;

		if ($vs->save()) {
			$verification = $twilio->verify->v2
				->services($serviceId)
				->verifications
				->create($recepient, "sms");
			
			return $verification->status;
		}
		
		return false;
    }

    /**
	* @function checkVerificationCode
	* @return string (pending | approved)
	*/
    public function checkVerificationCode($code, $recepient) {
    	if ($vs = VerificationService::where('recepient', $recepient)->first()) {
	    	$twilio = new Client($this->SID, $this->TOKEN);
			$verification_check = $twilio->verify->v2
				->services($vs->service_id)
				->verificationChecks
				->create($code, ["to" => $recepient] );
			
			return $verification_check->status;
    	}

    	return false;
    }
}