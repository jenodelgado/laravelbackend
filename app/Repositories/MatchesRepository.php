<?php
/**
* @func getList | list of all matches without stats
* @author Joan Villariaza - de los Santos
**/

namespace App\Repositories;

use App\Repositories\Repository;

class MatchesRepository extends Repository
{
    public function getList() {
    	return $this->call();
    }
}