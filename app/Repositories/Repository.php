<?php
/**
* I created this Base Repository for all the external data 
* like the sports API.
*
* @author Joan Villariaza (joan.villariaza@gmail.com)
*/

namespace App\Repositories;

class Repository
{
	/**
	* @var API_URL
	*/
	protected $API_URL;

	/**
	* @var API_KEY
	*/
	protected $API_KEY;	

	/**
	* @var API_SECRET
	*/
	protected $API_SECRET;	

	/**
	* @var API_HOST
	*/
	protected $API_HOST;	

	function __construct() {
		$this->API_URL    = env('API_URL'    , '');
		$this->API_KEY    = env('API_KEY'    , '');
		$this->API_SECRET = env('API_SECRET' , '');
		$this->API_HOST   = env('API_HOST'   , '');
	}

	/**
	* I added this initializer in case they want to add 
	* their own url and credentials inside the repository 
	* instead of environment file.
	* @param url
	* @param key
	* @param secret
	* @param host 
	*/
	function init(
		$url=false, 
		$key=false,
		$secret=false,
		$host=false
	) {
		$this->API_URL    = (!$url)    ? env('API_URL'    , '') : $url;
		$this->API_KEY    = (!$key)    ? env('API_KEY'    , '') : $key;
		$this->API_SECRET = (!$secret) ? env('API_SECRET' , '') : $secret;
		$this->API_HOST   = (!$host)   ? env('API_HOST'   , '') : $host;
	}

	function call() {
		try {
			$ch = curl_init($this->API_URL);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_ENCODING,"");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept: application/json',
				'Content-Type: application/json'));

			$response = curl_exec($ch);
			$json = json_decode($response, true);
			
			curl_close($ch);

			return $json;

		} catch (HttpException $ex) {
			echo $ex;
		}
		
	}
}