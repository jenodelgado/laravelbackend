<?php
/**
* @func getList | list of all matches without stats
* @author Joan Villariaza - de los Santos
**/

namespace App\Repositories;

use App\Repositories\Repository;

class StreamsRepository extends Repository
{
    public function getVideo($channel, $action, $ip) {
    	$this->init(env('STREAM_URL', '').'&ch='.$channel.'&ip='.$ip.'&action='.$action);
    	return $this->call();
    }
}