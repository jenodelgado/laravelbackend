<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerificationService extends Model
{
    /**
    * @var database table
    */
    protected $table = "verification_services";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recepient',
        'service',
        'service_id'
    ];

}
