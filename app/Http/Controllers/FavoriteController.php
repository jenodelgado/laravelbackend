<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Favorites;
use Validator;
use DB;
use Illuminate\Validation\Rule;

class FavoriteController extends Controller
{

    public function insertFav(Request $request){
        $rules = [
            'isClick'=>'required|int',
            'user_id'=>'required|int',
            'league'=>'required|string'
        ];

        $errorMessage = [
            'required' => 'Enter your :attribute first.'
        ];
        $this->validate($request, $rules, $errorMessage);

        Favorites::create([
            'isClick' => $request->isClick,
            'user_id' => $request->user_id,
            'league' => strtolower($request->league)
        ]);

    }

    public function get_user_favs(Favorites $favorites){
        $favorites = Favorites::toBase()->get();
        return $favorites;
    }

    public function edit($id){
        $favorites = Favorites::find($id);
        return $favorites;
    }

    // public function update(Request $request,$id){
    //     $rules = [
    //         'isClick'=>'required|int'
    //     ];
    // }

    public function update(Request $request,$id){
        $data = $request->except();

        $validator = Validator::make($request->all(), [
        'isClick' => 'required|int',
        ]);

        if ($validator->fails()) {
            return redirect()->Back()->withInput()->withErrors($validator);
        }
        $favorites = Favorites::find($id);

        if($favorites->update($data)){
            // Session::flash('message', 'Update successfully!');
            // Session::flash('alert-class', 'alert-success');
            // return redirect()->route('subjects');
        }else{
            // Session::flash('message', 'Data not updated!');
            // Session::flash('alert-class', 'alert-danger');
        }

        return Back()->withInput();
    }
}
