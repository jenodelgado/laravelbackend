<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SMSRepository as SMS;

class SMSController extends Controller
{
    public function sendOTP(Request $request) {
		$all = $request->all();
		if (!$all['recepient'])
			return false;

		$SMS = new SMS("NINETY SIX GROUP OTP");
		
		if ($status = $SMS->sendVerificationToken('+'.$all['recepient']))
			return [
				'status' => $status,
				'message' => "Token verification sent."
			];

		return false;
	}

    public function checkCode(Request $request) {
		$all = $request->all();

		if (!$all['recepient'] || !$all['code'])
			return false;

		$SMS = new SMS("NINETY SIX GROUP OTP");

		try {
			$status  = $SMS->checkVerificationCode($all['code'], '+'.$all['recepient']);
		} catch(Exception $e) {
			return [
				'status' => 'error',
				'message' => $e->getMessage()
			];
		}

		$success = "The code has been successfully verified.";
		$error   = "The code added is incorrect.";
		
		return [
			'status'  => $status,
			'message' => ($status == 'approved') ? $success : $error
		];
	}
}
