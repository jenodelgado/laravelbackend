<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use JWTAuth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;


class AuthController extends Controller
{
    //
    protected $user;
    public function __construct()
    {
        $this->user = new User;
    }


    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),
        [
            'firstname'=>'required|string',
            'lastname'=>'required|string',
            'username'=>'required|string',
            'email'=>'required|string',
            'password'=>'required|string|min:6',
        
        ]);

        if($validator->fails())
        {
            return response()->json([
                "success"=>false,
                "message"=>$validator->messages()->toArray(),
            ],400);
        }

        $check_email = $this->user->where("email", $request->email)->count();
        if($check_email > 0)
        {
            return response()->json([
            'success'=>false,
            'message'=>'this email already exist please try another email'
            ],200);
        }

        $check_username = $this->user->where("username", $request->username)->count();
        if($check_username > 0)
        {
            return response()->json([
            'success'=>false,
            'message'=>'this username already exist please try username'
            ],200);
        }

        $registerComplete = $this->user::create([
            'firstname'=>$request->firstname,
            'lastname'=>$request->lastname,
            'username'=>$request->username,
            'email'=>$request->email,
            'password'=> Hash::make($request->password),
            
        ]);
        

        if($registerComplete)
            return $this->login($request);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->only('username', 'password'),
        [
            'username'=>'required|string',
            'password'=>'required|string',
        ]
        );

        if($validator->fails())
        {
            return response()->json([
                "success"=>false,
                "message"=>$validator->messages()->toArray(),
            ],400);
        }

        $jwt_token = null;

        $input = $request->only("username","password");

        if(!$jwt_token = auth('users')->attempt($input))
        {
            return response()->json([
                'success'=>false,
                'message'=>'invalid username or password'
            ]);
        }

        return response()->json([
            'success'=>true,
            'token'=>$jwt_token,
        ]);
    }
}
