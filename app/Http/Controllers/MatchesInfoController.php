<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\MatchesInfoRepository;

class MatchesInfoController extends Controller
{
    public function getList(Request $request) {
    	$matchesinfo = new MatchesInfoRepository;
    	return $matchesinfo->getList();
    }
}