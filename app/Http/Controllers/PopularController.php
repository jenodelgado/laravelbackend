<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\PopularRepository;

class PopularController extends Controller
{
    public function getList(Request $request) {
    	$popular = new PopularRepository;
    	return $popular->getList();
    }
}