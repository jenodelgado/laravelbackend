<?php
/**
*
* @author Joan Villariaza - de los Santos
**/

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\StreamsRepository;

class StreamsController extends Controller
{
    public function getByChannel(Request $request) {
    	$channel = $request->route('channel');
    	$action  = $request->route('action');
    	$ip      = $request->route('ip');

    	if (!$channel || !$action || !$ip)
    		return false;

    	$stream = new StreamsRepository;
    	return json_encode($stream->getVideo($channel, $action, $ip));
    }
}