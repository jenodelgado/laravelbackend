<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\H2hRepository;

class H2hController extends Controller
{
    public function getList(Request $request) {
    	$h2h = new H2hRepository;
    	return $h2h->getList();
    }
}