<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\LeagueRepository;

class LeagueController extends Controller
{
    public function getList(Request $request) {
    	$league = new LeagueRepository;
    	return $league->getList();
    }
}