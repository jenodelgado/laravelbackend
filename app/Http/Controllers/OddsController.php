<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\OddsRepository;

class OddsController extends Controller
{
    public function getList(Request $request) {
    	$odds = new OddsRepository;
    	return $odds->getList();
    }
}