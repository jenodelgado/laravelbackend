<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\StatsRepository;

class StatsController extends Controller
{
    public function getList(Request $request) {
    	$stats = new StatsRepository;
    	return $stats->getList();
    }
}