<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\ShortStatsRepository;

class ShortStatsController extends Controller
{
    public function getList(Request $request) {
    	$shortstats = new ShortStatsRepository;
    	return $shortstats->getList();
    }
}