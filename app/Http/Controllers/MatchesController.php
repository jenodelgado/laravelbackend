<?php
/**
*
* @author Joan Villariaza - de los Santos
**/

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\MatchesRepository;

class MatchesController extends Controller
{
    public function getList(Request $request) {
    	$match = new MatchesRepository;
    	return json_encode($match->getList());
    }
}