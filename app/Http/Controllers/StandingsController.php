<?php


namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\StandingRepository;

class StandingsController extends Controller
{
    public function getList(Request $request) {
    	$match = new StandingRepository;
    	return $match->getList();
    }
}