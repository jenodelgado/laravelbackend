<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { echo 'You do not have any rights to access this page.'; });

// ========= VERSION 1 ========= //
Route::group([ 'prefix' => 'v1' ], function(){

	Route::get('/', function(){ echo "API Version 1"; });

	// matches
	Route::get('/matches', 'MatchesController@getList');

	// live stream
	Route::get('/stream/{channel}/{ip}/{action}', 'StreamsController@getByChannel');

	// OTP
	Route::get('/otp/send', 'SMSController@sendOTP');
	Route::get('/otp/check', 'SMSController@checkCode');
	
});
