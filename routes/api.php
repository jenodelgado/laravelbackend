<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group([
    'prefix'=>'user',
    'namespace'=>'User'
],
function(){
    Route::post('register','AuthController@register');
    Route::post('login', 'AuthController@login');
    
}

);
Route::post('favorite', 'FavoriteController@insertFav');
Route::get('favorite', 'FavoriteController@get_user_favs');
Route::post('favorite/update/{id}', 'FavoriteController@update');
Route::get('favorite/getfavorite/{id}', 'FavoriteController@edit');
